#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PySide6.QtCore import QObject, Signal

from Archiver import Archiver

from filecmp import dircmp

import os
import shutil


class Worker(QObject):

    finished = Signal()
    message = Signal(str)
    target_error = Signal()

    def __init__(self, command='move'):
        QObject.__init__(self)

        self.source_path = None
        self.target_path = None
        self.command = command

    def set_source_dir(self, source_dir):
        self.source_path = source_dir

    def set_target_dir(self, target_dir):
        self.target_path = target_dir

    def execute(self):
        if self.command == 'move':
            self.__execute_copy()
        if self.command == 'archive':
            self.__execute_archive()

    def __execute_copy(self):
        print(f'Move from {self.source_path} to {self.target_path}')
        self.message.emit('Start moving')

        is_valid = self.__is_target_free()
        if not is_valid:
            self.target_error.emit()

        if is_valid:
            is_valid = self.__copy_dir()
        if is_valid:
            is_valid = self.__check_dir()
        if is_valid:
            self.__remove_dir()

        self.message.emit('Finished')

        if not is_valid:
            self.message.emit('Finished with errors')

        self.finished.emit()

    def __execute_archive(self):
        self.message.emit('Start archiving')

        archiver = Archiver()
        archiver.set_source_path(self.source_path)
        archiver.set_target_path(self.target_path)
        is_valid = archiver.execute()

        self.message.emit('Finished')
        if not is_valid:
            self.message.emit('Finished with errors')

        self.finished.emit()

    def __is_target_free(self) -> bool:
        return not os.path.exists(self.target_path)

    def __copy_dir(self):
        self.message.emit('Copying')
        result = False
        try:
            shutil.copytree(self.source_path, self.target_path)
            result = True
        except OSError as exc:
            print('Exception: ', exc)

        return result

    def __check_dir(self) -> bool:
        self.message.emit('Copying')

        compare_ok = self.is_same(self.source_path, self.target_path)

        print('Identical:', compare_ok)

        return compare_ok

    def __remove_dir(self):
        self.message.emit('Removing')

        try:
            shutil.rmtree(self.source_path)
        except OSError as e:
            print("Error: %s - %s." % (e.filename, e.strerror))

    def is_same(self, dir1, dir2) -> bool:
        compared = dircmp(dir1, dir2)
        if compared.left_only or compared.right_only or compared.diff_files or compared.funny_files:
            return False
        for subdir in compared.common_dirs:
            if not self.is_same(os.path.join(dir1, subdir), os.path.join(dir2, subdir)):
                return False
        return True

