#!/bin/sh
pyside6-uic MainWidget.ui > Ui_MainWidget.py
pyside6-uic SettingsWidget.ui > Ui_SettingsWidget.py
pyside6-uic IntegrityWidget.ui -o Ui_IntegrityWidget.py
pyside6-rcc Icons.qrc > Icons_rc.py
