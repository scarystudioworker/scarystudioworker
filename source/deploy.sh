#!/bin/sh
pyside6-uic MainWidget.ui > Ui_MainWidget.py
# pyside2-rcc icons.qrc > icons_rc.py

./compile.sh
mkdir -p ../Deploy
cp ./Archiver.py ../Deploy/
cp ./main.py ../Deploy/
cp ./Icons_rc.py ../Deploy/
cp ./IntegrityWidget.py ../Deploy
cp ./MainWidget.py ../Deploy/
cp ./Ui_IntegrityWidget.py ../Deploy
cp ./Ui_MainWidget.py ../Deploy/
cp ./Ui_SettingsWidget.py ../Deploy/
cp ./Worker.py ../Deploy/
cp ./SettingsWidget.py ../Deploy/

#

cp ./icons/Icon.svg ../Deploy/
