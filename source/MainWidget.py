#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide6.QtWidgets import QFileSystemModel, QMenu, QMessageBox, QVBoxLayout, QWidget
from PySide6.QtCore import QCoreApplication, QPoint, QThread, Slot

from IntegrityWidget import IntegrityWidget
from SettingsWidget import SettingsWidget
from Worker import Worker

from Ui_MainWidget import Ui_MainWidget

import subprocess


def init_model(filepath, view):
    model = QFileSystemModel()
    model.setRootPath(filepath)
    view.setModel(model)
    view.setRootIndex(model.index(filepath))


def open_folder(path1, path2):
    subprocess.Popen(['dolphin', '--split', path1, path2], start_new_session=True)


class MainWidget(QWidget):

    ui = Ui_MainWidget()

    # test = True
    test = False

    def __init__(self):
        QWidget.__init__(self)

        # Attributes
        self.thread = None
        self.is_idle = True

        # Initialisation
        self.ui.setupUi(self)

        self.ui.treeViewSong.setAccessibleName('Song-Widget')
        self.ui.treeViewCurrent.setAccessibleName('Current-Widget')
        self.__init_ui()
        self.__init_connections()

    def __init_ui(self):
        version = QCoreApplication.applicationVersion()
        title = self.windowTitle()
        self.setWindowTitle(f'{title} - {version}')
        self.__init_settings_tab()
        self.__update_paths()
        init_model(self.current_songs_path, self.ui.treeViewCurrent)
        init_model(self.old_songs_path, self.ui.treeViewSong)
        self.ui.treeViewCurrent.header().resizeSection(0, 200)
        self.ui.treeViewSong.header().resizeSection(0, 200)
        self.ui.labelCurrent.setText(self.current_songs_path)
        self.ui.labelOldSong.setText(self.old_songs_path)
        self.__init_integrity_tab()

    def __init_settings_tab(self):
        layout = QVBoxLayout()
        settings_widget = SettingsWidget(self.test)
        layout.addWidget(settings_widget)
        self.ui.tabSettings.setLayout(layout)

    def __init_integrity_tab(self):
        layout = QVBoxLayout()
        integrity_widget = IntegrityWidget(self.test)
        layout.addWidget(integrity_widget)
        self.ui.tabIntegrity.setLayout(layout)
        integrity_widget.current_path = self.current_songs_path
        integrity_widget.song_path = self.old_songs_path

    def __init_connections(self):
        self.ui.treeViewCurrent.customContextMenuRequested.connect(self.__open_context_menu_current)
        self.ui.treeViewSong.customContextMenuRequested.connect(self.__open_context_menu_old)
        self.ui.tabWidget.currentChanged.connect(self.__update_integrity)

    def __update_paths(self):
        settings_widget = self.ui.tabSettings.layout().itemAt(0).widget()
        self.current_songs_path = settings_widget.current_path()
        self.old_songs_path = settings_widget.songs_path()
        self.archive_path = settings_widget.archive_path()

    @Slot(QPoint)
    def __open_context_menu_current(self, pos):
        global_pos = self.ui.treeViewCurrent.viewport().mapToGlobal(pos)

        self.__open_menu(global_pos, pos, 'Move back', self.ui.treeViewCurrent)

    @Slot(QPoint)
    def __open_context_menu_old(self, pos):
        global_pos = self.ui.treeViewSong.viewport().mapToGlobal(pos)

        self.__open_menu(global_pos, pos, 'Move to current', self.ui.treeViewSong)

    @Slot(int)
    def __update_integrity(self, index):
        integrity_index = self.ui.tabWidget.indexOf(self.ui.tabIntegrity)
        if integrity_index == index:
            widget = self.ui.tabIntegrity.layout().itemAt(0).widget()
            if widget:
                widget.run()

    def __open_menu(self, global_pos, pos, command, source_widget):
        if self.is_idle:
            menu = QMenu()
            menu.addAction(command)
            menu.addAction('Open')
            menu.addAction('Archive')
            action = menu.exec_(global_pos)
            if action:
                self.__perform_action(action, pos, global_pos, source_widget)

    def __perform_action(self, action, pos, global_pos, source_widget):
        if action.text() == 'Move back':
            self.__move_back(pos)
        if action.text() == 'Move to current':
            self.__move_to_current(pos)
        if action.text() == 'Open':
            open_folder(self.current_songs_path, self.old_songs_path)
        if action.text() == 'Archive':
            self.__archive_folder(pos, source_widget)

    def __move_back(self, pos):
        model_index = self.ui.treeViewCurrent.indexAt(pos)
        dir_name = self.ui.treeViewCurrent.model().fileName(model_index)
        source_path = f'{self.current_songs_path}/{dir_name}'
        target_path = f'{self.old_songs_path}/{dir_name}'

        try:
            self.__initialize_worker(source_path, target_path, 'move')
            self.is_idle = False
            self.thread.start()
        except:  # TODO
            print('Exception')

    def __move_to_current(self, pos):
        model_index = self.ui.treeViewSong.indexAt(pos)
        dir_name = self.ui.treeViewSong.model().fileName(model_index)
        source_path = f'{self.old_songs_path}/{dir_name}'
        target_path = f'{self.current_songs_path}/{dir_name}'

        try:
            self.__initialize_worker(source_path, target_path, 'move')
            self.is_idle = False
            self.thread.start()
        except:  # TODO
            print('Exception')

    def __archive_folder(self, pos, source_widget):
        model_index = source_widget.indexAt(pos)
        source_path = source_widget.model().filePath(model_index)
        print('Source-Dir', source_path)
        target_path = self.archive_path
        print('Target-Dir', target_path)

        try:
            self.__initialize_worker(source_path, target_path, 'archive')
            self.is_idle = False
            self.thread.start()
        except:  # TODO
            print('Exception')

    def __initialize_worker(self, source, target, command):
        self.thread = QThread()
        self.worker = Worker(command)
        self.worker.set_source_dir(source)
        self.worker.set_target_dir(target)

        self.worker.moveToThread(self.thread)

        connected = self.worker.message.connect(self.show_output)
        assert connected
        connected = self.worker.target_error.connect(self.__display_target_error)
        assert connected

        connected = self.thread.started.connect(self.worker.execute)
        assert connected
        connected = self.worker.finished.connect(self.thread.quit)
        assert connected
        connected = self.worker.finished.connect(self.worker.deleteLater)
        assert connected
        connected = self.thread.finished.connect(self.thread.deleteLater)
        assert connected

        # debug
        connected = self.worker.finished.connect(self.__set_idle)
        assert connected
        # connected = self.worker.destroyed.connect(lambda: print('Worker destroyed'))
        # assert connected
        connected = self.thread.destroyed.connect(lambda: print('Thread destroyed'))
        assert connected

        self.ui.progressBar.setMaximum(0)

    @Slot(str)
    def show_output(self, message):
        self.ui.labelInfo.setText(message)

    @Slot()
    def __display_target_error(self):
        QMessageBox.critical(self, 'Error', 'Target is already existing')

    @Slot()
    def __set_idle(self):
        self.is_idle = True
        self.ui.progressBar.setMaximum(100)
