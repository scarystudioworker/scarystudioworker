from PySide6.QtWidgets import QTreeWidgetItem, QWidget

from Ui_IntegrityWidget import Ui_InregrityWidget

import os


class IntegrityWidget(QWidget):

    ui = Ui_InregrityWidget()

    def __init__(self, is_test_case):
        QWidget.__init__(self)

        self.ui.setupUi(self)
        self.__currentPath = None
        self.__songPath = None

    def run(self):
        self.__update_dublicates()

    @property
    def current_path(self):
        return self.__currentPath

    @current_path.setter
    def current_path(self, value):
        self.__currentPath = value
        self.ui.labelCurrentFolder.setText(value)

    @property
    def song_path(self):
        return self.__currentPath

    @song_path.setter
    def song_path(self, value):
        self.__songPath = value
        self.ui.labelSongFolder.setText(value)

    def __update_dublicates(self):
        self.ui.treeWidget.clear()

        list_current = [f for f in os.listdir(self.__currentPath) if not f.startswith('.')]
        list_song = [f for f in os.listdir(self.__songPath) if not f.startswith('.')]
        list_result = []
        for folder_name in list_current:
            if folder_name in list_song:
                list_result.append(folder_name)

        list_result.sort()

        for folder_name in list_result:
            self.__make_entry(folder_name)

    def __make_entry(self, folder_name):
        item = QTreeWidgetItem(self.ui.treeWidget)
        item.setText(0, folder_name)
