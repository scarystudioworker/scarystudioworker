from datetime import datetime

import os
import tarfile

extention = 'xz'


def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:xz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))


def archive_name(source_path, target_path) -> str:
    now = datetime.now()
    time_date_Prefix = now.strftime("%d%m%Y-%H%M%S")
    dir_name = os.path.basename(source_path)
    result = f'{target_path}/{time_date_Prefix}-{dir_name}.{extention}'
    return result


class Archiver:
    def __init__(self):
        self.source_path_name = None
        self.target_path_name = None

    def set_source_path(self, source_path):
        self.source_path_name = source_path

    def set_target_path(self, target_path):
        self.target_path_name = target_path

    def execute(self) -> bool:
        target = archive_name(self.source_path_name, self.target_path_name)
        make_tarfile(target, self.source_path_name)
        return True


