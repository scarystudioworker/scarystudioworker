#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide6.QtCore import Signal
from PySide6.QtWidgets import QWidget

from Ui_SettingsWidget import Ui_SettingsWidget


class SettingsWidget(QWidget):

    ui = Ui_SettingsWidget()

    # Signals
    updated = Signal()

    def __init__(self, is_test_case):
        QWidget.__init__(self)

        self.ui.setupUi(self)

        # attributes
        self.test = is_test_case

        # init
        self.__init_paths()

        # connections
        is_connected = self.ui.lineEditCurrentPath.textChanged.connect(self.updated)
        assert is_connected

    def archive_path(self):
        return self.ui.lineEditArchivePath.text()

    def songs_path(self):
        return self.ui.lineEditSongsPath.text()

    def current_path(self):
        return self.ui.lineEditCurrentPath.text()

    def __init_paths(self):
        if not self.test:
            self.ui.lineEditCurrentPath.setText('/home/hallo/CurrentSongs')
            self.ui.lineEditSongsPath.setText('/home/hallo/Studio/Songs')
            self.ui.lineEditArchivePath.setText('/home/hallo/Studio/Songs/__Archives')
        else:
            self.ui.lineEditCurrentPath.setText('/home/hallo/Projects/ScaryStudioWorker/Test/CurrentSongs')
            self.ui.lineEditSongsPath.setText('/home/hallo/Projects/ScaryStudioWorker/Test/Songs')
            self.ui.lineEditArchivePath.setText('/home/hallo/Projects/ScaryStudioWorker/Test/Songs/__Archives')
