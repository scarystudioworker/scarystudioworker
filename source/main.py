#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PySide6.QtCore import QCoreApplication, QFile, QIODevice, QTextStream
from PySide6.QtWidgets import QApplication

from MainWidget import MainWidget

import sys
import os

# current_dir = os.path.dirname(__file__)


def __start():
    app = QApplication(sys.argv)

    QCoreApplication.setOrganizationName('Scary Hallo Soft')
    QCoreApplication.setOrganizationDomain('hallosoft.de')
    QCoreApplication.setApplicationName('ScaryStudioWorker')
    QCoreApplication.setApplicationVersion('1.0.0.11')

    # __load_skin(app)

    widget = MainWidget()
    widget.show()

    sys.exit(app.exec())


# not in use
def __load_skin(app):
    current_dir = os.getcwd()
    filepath = f'{current_dir}/Skins/darkstyle.qss'
    file = QFile(filepath)
    if file.open(QIODevice.ReadOnly | QIODevice.Text):
        stream = QTextStream(file)
        app.setStyleSheet(stream.readAll())
    else:
        print(f'Cannot open {filepath}')


if __name__ == "__main__":
    __start()
